package cz.vse.svoboda;

import cz.vse.svoboda.logika.Jidlo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Testovací třída JidloTest slouží ke komplexnímu otestování
 * třídy Jidlo
 *
 * @Project: zakladAdventuryIDEA
 * @Author: Petr Svoboda
 * @Ident: svop09
 */
public class JidloTest {

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Testuje zda je jídlo otrávené či nikoli
     */
    @Test
    public void testIsOtravena() {
        Jidlo vyvar = new Jidlo("vývar","kuřecí vývar jako od babičky",false);
        Jidlo svickova = new Jidlo("svíčková","Klasiká svíčková se šesti",true);
        assertFalse(vyvar.isOtravena());
        assertTrue(svickova.isOtravena());
    }
}
