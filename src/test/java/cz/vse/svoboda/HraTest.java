package cz.vse.svoboda;

import cz.vse.svoboda.logika.Hra;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída HraTest slouží ke komplexnímu otestování
 * třídy Hra
 *
 * @author    Jarmila Pavlíčková
 * @version  pro školní rok 2016/2017
 */
public class HraTest {
    private Hra hra1;

    //== Datové atributy (statické i instancí)======================================

    //== Konstruktory a tovární metody =============================================
    //-- Testovací třída vystačí s prázdným implicitním konstruktorem ----------

    //== Příprava a úklid přípravku ================================================

    /***************************************************************************
     * Metoda se provede před spuštěním každé testovací metody. Používá se
     * k vytvoření tzv. přípravku (fixture), což jsou datové atributy (objekty),
     * s nimiž budou testovací metody pracovat.
     */
    @Before
    public void setUp() {
        hra1 = new Hra();
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každé testovací metody.
     */
    @After
    public void tearDown() {
    }

    //== Soukromé metody používané v testovacích metodách ==========================

    //== Vlastní testovací metody ==================================================

    /***************************************************************************
     * Testuje průběh hry, po zavolání každěho příkazu testuje, zda hra končí
     * a v jaké aktuální místnosti se hráč nachází.
     * Při dalším rozšiřování hry doporučujeme testovat i jaké věci nebo osoby
     * jsou v místnosti a jaké věci jsou v batohu hráče.
     * 
     */
    @Test
    public void testPrubehHry() {
        assertEquals("restaurace", hra1.getHerniPlan().getAktualniProstor().getNazev());
        assertEquals(0,hra1.getHerniPlan().getAktualniProstor().getPocetSnedenych());
        hra1.getHerniPlan().getAktualniProstor().setPocetSnedenych(2);
        assertEquals(2,hra1.getHerniPlan().getAktualniProstor().getPocetSnedenych());
        hra1.zpracujPrikaz("jdi hospoda");
        assertFalse(hra1.konecHry());
        assertEquals("hospoda", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi bar");
        assertFalse(hra1.konecHry());
        assertEquals("hospoda", hra1.getHerniPlan().getAktualniProstor().getNazev());

        assertEquals(0,hra1.getPocetBodu());
        assertEquals(0,hra1.getPocetSnezenych());
        hra1.zpracujPrikaz("sněz pivo");
        assertFalse(hra1.getHerniPlan().getAktualniProstor().obsahujeJidlo("pivo"));
        assertTrue(hra1.getHerniPlan().getAktualniProstor().obsahujeJidlo("utopenec"));
        assertEquals(1,hra1.getPocetBodu());
        assertEquals(1,hra1.getPocetSnezenych());

        assertEquals(1,hra1.getPocetProtijedu());
        assertFalse(hra1.isSnedlOtravene());

        hra1.setPocetBodu(10);
        assertEquals(10,hra1.getPocetBodu());
        hra1.zpracujPrikaz("batoh koupit");
        assertEquals(5,hra1.getPocetBodu());
        assertEquals(2,hra1.getPocetProtijedu());

        hra1.zpracujPrikaz("rada utopenec");
        hra1.zpracujPrikaz("rada utopenec");
        hra1.zpracujPrikaz("rada utopenec");
        String text  = hra1.zpracujPrikaz("rada utopenec").split("\n")[0];
        assertEquals("Došli ti rady",text);

        hra1.zpracujPrikaz("konec");
        assertTrue(hra1.konecHry());
    }
}
