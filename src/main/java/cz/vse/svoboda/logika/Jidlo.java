package cz.vse.svoboda.logika;

/**
 * Trida Jidlo - popisuje jednotlivé jídla hry
 *
 * Tato třída je součástí jednoduché textové hry.
 *
 * "Jidlo" reprezentuje jedno jídlo (pokrm) ve scénáři hry.
 * Jídlo může být otrávené nebo nikoli.
 *
 * @Project: zakladAdventuryIDEA
 * @Author: Petr Svoboda
 * @Ident: svop09
 */
public class Jidlo {
    private final String nazev;
    private final String popis;
    private final boolean otravena;

    /**
     * Vytvoření jídla se zadaným popisem, názvem a pokud je otrávené
     *
     * @param nazev Název jídla
     * @param popis Popis jídla
     * @param otravena Je-li jídlo otrávené
     */
    public Jidlo(String nazev, String popis, boolean otravena) {
        this.nazev = nazev;
        this.popis = popis;
        this.otravena = otravena;
    }

    /**
     * Vrací název jídla
     *
     * @return název jídla
     */
    public String getNazev() {
        return nazev;
    }

    /**
     * Vrací popis jídla
     *
     * @return popis jídla
     */
    public String getPopis() {
        return popis;
    }

    /**
     * Vrací zda je jídlo otrávené jídla
     *
     * @return zda je jídlo otrávené jídla
     */
    public boolean isOtravena() {
        return otravena;
    }
}
