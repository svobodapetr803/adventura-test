package cz.vse.svoboda.logika;

/**
 *  Třída PrikazNapoveda implementuje pro hru příkaz napoveda.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 *@author     Jarmila Pavlickova, Luboš Pavlíček
 *@version    pro školní rok 2016/2017
 *  
 */
public class PrikazNapoveda implements IPrikaz {
    
    private static final String NAZEV = "nápověda";
    private SeznamPrikazu platnePrikazy;
    
    
     /**
    *  Konstruktor třídy
    *  
    *  @param platnePrikazy seznam příkazů,
    *                       které je možné ve hře použít,
    *                       aby je nápověda mohla zobrazit uživateli. 
    */    
    public PrikazNapoveda(SeznamPrikazu platnePrikazy) {
        this.platnePrikazy = platnePrikazy;
    }
    
    /**
     *  Vrací základní nápovědu po zadání příkazu "napoveda". Nyní se vypisuje
     *  vcelku primitivní zpráva a seznam dostupných příkazů.
     *  
     *  @return napoveda ke hre
     */
    @Override
    public String provedPrikaz(String... parametry) {
        return "Tvým úkolem je sníst nejméně 27 jídel a v každá místností sníst minimálně 2 jídla\n"
                + "celkově bude 11 otrávených jídel a 32 jídel kterých bude možné sníst.\n"
                + "V první třech místnostích budou 3 jídla, v dalších třech 4 jídla a v dalsích třech 5 jídel\n"
                + "V poslední bude 7 jídel a z toho budou 2 otrávená\n"
                + "Za každé snězené jídlo dostaneš jeden bod. Za 5 bodů lze koupit proti jed v batoh koupit.\n"
                + "Aby proti jed fungoval správné, musíš ho požit před snězením jídla.\n"
                + "Snězením jídla otráveného nebo nikoli, účinek vyprchá. Protijed sníš zadáním sněz protijed.\n"
                + "\n"
                + "Můžeš zadat tyto příkazy:\n"
                + platnePrikazy.vratNazvyPrikazu()
                + "\n"
                + "Pro zjištění počtu zbývajícíh rad zadej: rada počet";
    }
    
     /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
      public String getNazev() {
        return NAZEV;
     }

}
