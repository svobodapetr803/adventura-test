package cz.vse.svoboda.logika;

/**
 *  Třída Hra - třída představující logiku adventury.
 * 
 *  Toto je hlavní třída  logiky aplikace.  Tato třída vytváří instanci třídy HerniPlan, která inicializuje mistnosti hry
 *  a vytváří seznam platných příkazů a instance tříd provádějící jednotlivé příkazy.
 *  Vypisuje uvítací a ukončovací text hry.
 *  Také vyhodnocuje jednotlivé příkazy zadané uživatelem.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 */

public class Hra implements IHra {
    private final SeznamPrikazu platnePrikazy;    // obsahuje seznam přípustných příkazů
    private final HerniPlan herniPlan;
    private boolean konecHry = false;
    private boolean snedlOtravene = false;
    private int pocetSnezenych;
    private int pocetBodu;
    private int pocetProtijedu;

    /**
     *  Vytváří hru a inicializuje místnosti (prostřednictvím třídy HerniPlan) a seznam platných příkazů.
     */
    public Hra() {
        herniPlan = new HerniPlan();
        pocetSnezenych = 0;
        pocetBodu = 0;
        pocetProtijedu = 1;
        platnePrikazy = new SeznamPrikazu();
        platnePrikazy.vlozPrikaz(new PrikazNapoveda(platnePrikazy));
        platnePrikazy.vlozPrikaz(new PrikazJdi(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazKonec(this));
        platnePrikazy.vlozPrikaz(new PrikazZnovu(this));
        platnePrikazy.vlozPrikaz(new PrikazSnez(herniPlan, this));
        platnePrikazy.vlozPrikaz(new PrikazRada(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazBody(this));
        platnePrikazy.vlozPrikaz(new PrikazBatoh(this));
    }

    /**
     *  Vrátí úvodní zprávu pro hráče.
     */
    public String vratUvitani() {
        return "Vítejte!\n"
                + "Tvým úkolem je sníst nejméně 27 jídel a v každé místností sníst minimálně 2 jídla\n"
                + "celkově bude 11 otrávených jídel a 32 jídel kterých bude možné sníst.\n"
                + "V první třech místnostích budou 3 jídla, v dalších třech 4 jídla a v dalších třech 5 jídel\n"
                + "V poslední místnosti bude 7 jídel a z toho budou 2 otrávená\n"
                + "Pokud sníte otrávené jídlo prohráváte. Pokud sníte daný počet jídel vyhráváte. \n"
                + "Za každé jídlo dostaneš jeden bod. Za 5 bodů je možné si koupit proti jed.\n"
                + "\n" +
                herniPlan.getAktualniProstor().dlouhyPopis();
    }
    
    /**
     *  Vrátí závěrečnou zprávu pro hráče.
     */
    public String vratEpilog() {
        return "Dík, že jste si zahráli.  Ahoj.";
    }
    
    /** 
     * Vrací true, pokud hra skončila.
     */
     public boolean konecHry() {
        return konecHry;
    }

    /**
     *  Metoda zpracuje řetězec uvedený jako parametr, rozdělí ho na slovo příkazu a další parametry.
     *  Pak otestuje zda příkaz je klíčovým slovem  např. jdi.
     *  Pokud ano spustí samotné provádění příkazu.
     *  Jemně jsem ji upravil, tak aby se pokaždém příkazu znovu vypsalo info o místnosti
     *
     *@param  radek  text, který zadal uživatel jako příkaz do hry.
     *@return          vrací se řetězec, který se má vypsat na obrazovku
     */
    @Override
     public String zpracujPrikaz(String radek) {
        String [] slova = radek.split("[ \t]+");
        String slovoPrikazu = slova[0];
        String []parametry = new String[slova.length-1];
        for(int i=0 ;i<parametry.length;i++) {
            parametry[i] = slova[i + 1];
        }
        String textKVypsani;
        if (platnePrikazy.jePlatnyPrikaz(slovoPrikazu)) {
            IPrikaz prikaz = platnePrikazy.vratPrikaz(slovoPrikazu);

            if(prikaz != platnePrikazy.vratPrikaz("konec") && !snedlOtravene) {
                textKVypsani = prikaz.provedPrikaz(parametry);
                if(snedlOtravene){
                    textKVypsani += "\nNastal konec hry" +
                            "\nZadej konec pro ukončení hry nebo znovu pro restart hry" +
                            "\nZískal jsi: " + getPocetBodu() + " bodů" +
                            "\nSnědl jsi: " + getPocetSnezenych() + " jídel";
                    platnePrikazy.odeberPrikazy(new String[]{"jdi", "napoveda", "snez", "rada", "batoh"});
                } else if (pocetSnezenych >= 27 && prikaz.equals("sněz")) {
                    textKVypsani += "\n\n Vyhrál jsi, nyní můžeš pokračovat, aby jsi získal vyšší skóre. \n\n" + herniPlan.getAktualniProstor().dlouhyPopis();
                } else {
                    textKVypsani += "\n\n" + herniPlan.getAktualniProstor().dlouhyPopis();
                }
            } else {
                textKVypsani = prikaz.provedPrikaz(parametry);
            }
        }
        else if(snedlOtravene) {
            textKVypsani="Nastal konec hry\nZadej konec pro ukončení hry nebo znovu pro restart hry";
        }
        else {
            textKVypsani="Nevím co tím myslíš? Tento příkaz neznám. \n\n" + herniPlan.getAktualniProstor().dlouhyPopis();

        }
        return textKVypsani;
    }
    
    
     /**
     *  Nastaví, že je konec hry, metodu využívá třída PrikazKonec,
     *  mohou ji použít i další implementace rozhraní Prikaz.
     *  
     *  @param  konecHry  hodnota false= konec hry, true = hra pokračuje
     */
    void setKonecHry(boolean konecHry) {
        this.konecHry = konecHry;
    }
    
     /**
     *  Metoda vrátí odkaz na herní plán, je využita hlavně v testech,
     *  kde se jejím prostřednictvím získává aktualní místnost hry.
     *  
     *  @return     odkaz na herní plán
     */
     public HerniPlan getHerniPlan(){
        return herniPlan;
     }

    /**
     * Setter a getter pro zjištění zda hráč snědl otrávené jídlo
     *
     * @param snedlOtravene boolean
     */
    public void setSnedlOtravene(boolean snedlOtravene) {
        this.snedlOtravene = snedlOtravene;
    }

    public boolean isSnedlOtravene() {
        return snedlOtravene;
    }

    /**
     * Gettery a settery pro počty
     */
    public int getPocetSnezenych(){
         return pocetSnezenych;
    }

    public void setPocetSnezenych(int pocetSnezenych) {
        this.pocetSnezenych = pocetSnezenych;
    }

    public int getPocetBodu() {
        return pocetBodu;
    }

    public void setPocetBodu(int pocetBodu) {
        this.pocetBodu = pocetBodu;
    }

    public int getPocetProtijedu() {
        return pocetProtijedu;
    }

    public void setPocetProtijedu(int pocetProtijedu) {
        this.pocetProtijedu = pocetProtijedu;
    }
}

