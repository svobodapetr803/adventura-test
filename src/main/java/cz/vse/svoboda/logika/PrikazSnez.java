package cz.vse.svoboda.logika;

/**
 *  Třída PrikazSnez implementuje pro hru příkaz sněz.
 *  Tato třída je součástí jednoduché textové hry.
 *
 * @Project: zakladAdventuryIDEA
 * @Author: Petr Svoboda
 * @Ident: svop09
 */
public class PrikazSnez implements IPrikaz {

    private static final String NAZEV = "sněz";
    private HerniPlan plan;
    private Hra hra;
    private boolean imunita = false;

    /**
     * Konstruktor
     *
     * @param plan plán hry pro zjistění v jaké jsme místnosti
     * @param hra hra jako taková, nastavování jednotlivých parametrů hry, body, počet snědených...
     */
    public PrikazSnez(HerniPlan plan, Hra hra) {
        this.hra = hra;
        this.plan = plan;
    }

    /**
     * Provádí příkaz "sněz". Zjišťuje zda je jídlo otrávené či nikoli.
     * Pokud sní protijed, nastaví se mu imunita na jedno jídlo.
     * Pokud zadá příkaz sněz počet, vypíše kolik jídel ještě musí celkem sníst.
     * Sní dobré jídlo dostane body, sní špatné, tak prohává.
     *
     * @param parametry počet parametrů závisí na konkrétním příkazu.
     * @return zpráva, kterou vypíše hra hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Nevím co mám sníst";
        }

        // Jaké jídlo se má sníst
        String nazevJidla = parametry[0];
        // Aktuální prostor
        Prostor aktualniProstor = plan.getAktualniProstor();

        if(nazevJidla.equals("počet")){
            return 27-hra.getPocetSnezenych() > 0 ? "Ještě musíš sníst: " + (27-hra.getPocetSnezenych()) + " jídel" : "Už jsi snědl dostatečný počet jídel.";
        } else if(nazevJidla.equals("protijed") && hra.getPocetProtijedu() > 0){
            hra.setPocetProtijedu(hra.getPocetProtijedu()-1);
            imunita = true;
            return "Snědl jsi protijed";
        } else if (!aktualniProstor.obsahujeJidlo(nazevJidla)) {
            return "Jídlo s názvem " + nazevJidla + " v prostoru neexistuje";
        } else {
            Jidlo jidlo = aktualniProstor.vratJidlo(nazevJidla);
            if(jidlo.isOtravena() && imunita){
                aktualniProstor.setPocetSnedenych(aktualniProstor.getPocetSnedenych() + 1);
                aktualniProstor.odeberJidlo(nazevJidla);
                imunita = false;
                return "Snědl jsi otrávené jídlo, ale předtím jsi požil protijed a tak přežíváš";
            }else if (jidlo.isOtravena()) {
                aktualniProstor.odeberJidlo(nazevJidla);
                hra.setSnedlOtravene(true);
                return "Snědl jsi jídlo: " + jidlo.getPopis() + " a buhužel bylo otrávené a tím prohráváš\nKonec hry.";
            } else {
                aktualniProstor.setPocetSnedenych(aktualniProstor.getPocetSnedenych() + 1);
                aktualniProstor.odeberJidlo(nazevJidla);
                hra.setPocetSnezenych(hra.getPocetSnezenych()+1);
                hra.setPocetBodu(hra.getPocetBodu()+1);
                imunita = false;
                return "Snědl jsi jídlo: " + jidlo.getPopis() + " a bylo v pořádku";
            }
        }
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }
}
