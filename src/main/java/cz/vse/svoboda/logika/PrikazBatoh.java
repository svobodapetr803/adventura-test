package cz.vse.svoboda.logika;

/**
 *  Třída PrikazBatoh implementuje pro hru příkaz batoh.
 *  Tato třída je součástí jednoduché textové hry.
 *
 * @Project: zakladAdventuryIDEA
 * @Author: Petr Svoboda
 * @Ident: svop09
 */
public class PrikazBatoh implements IPrikaz {
    private static final String NAZEV = "batoh";
    private Hra hra;

    /**
     * Konstruktor
     *
     * @param hra hra jako taková, nastavování jednotlivých parametrů hry, body, počet snědených...
     */
    public PrikazBatoh(Hra hra){
        this.hra = hra;
    }

    /**
     * Vyhodnotí zda hráč zadal pouze příkaz batoh anebo batoh koupit.
     * Zadá-li batoh vypíše obsah batohu (počet protijedů)
     * Zadá-li batoh koupit pokusí se koupit jeden protijed.
     * pokud má hráč dost bodů, protijed koupí a odečte body,
     * pokud nemá, vráti hráči zprávu o nedostatku bodů.
     *
     * @param parametry počet parametrů závisí na konkrétním příkazu.
     * @return zpráva, kterou vypíše hra hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        int body = hra.getPocetBodu();
        int protijedy = hra.getPocetProtijedu();

        if(parametry.length == 0){
            return "V batohu je: " + protijedy + (protijedy == 0 || protijedy > 4 ? " protijedů" : (protijedy == 1 ? " protijed" : " protijedy"));
        } else {
            String akce = parametry[0];

            if(akce.equals("koupit") && body>=5){
                hra.setPocetBodu(body-5);
                hra.setPocetProtijedu(protijedy+1);
                return "Úspěšně si koupil protijed";
            } else if(akce.equals("koupit")) {
                return "Nemáš dostatek bodů pro koupi protijedu.";
            } else {
                return "Zadáváš příliš slov nebo neznámé druhé slovo.";
            }
        }
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }
}
