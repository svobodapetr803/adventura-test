package cz.vse.svoboda.logika;

/**
 *  Třída PrikazBody implementuje pro hru příkaz body.
 *  Tato třída je součástí jednoduché textové hry.
 *
 * @Project: zakladAdventuryIDEA
 * @Author: Petr Svoboda
 * @Ident: svop09
 */
public class PrikazBody implements IPrikaz {
    private static final String NAZEV = "body";
    private Hra hra;
    private int body;

    /**
     * Konstruktor
     *
     * @param hra hra jako taková, zjištění počtu bodů
     */
    public PrikazBody(Hra hra) {
        this.hra = hra;
    }

    @Override
    public String provedPrikaz(String... parametry) {
        body = hra.getPocetBodu();
        if (parametry.length == 0) {
            // podmínk ? platí : neplatí, slouží místo používání ifů pro boolean, velké zjednodušení
            return "Nyní máš " + body + (body == 0 || body > 4 ? " bodů" : (body == 1 ? " bod" : " body"));
        } else {
            return "Nevím proč zadaváš druhé slovo.";
        }
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }
}
