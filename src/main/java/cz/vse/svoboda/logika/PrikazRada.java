package cz.vse.svoboda.logika;

import java.util.*;

/**
 *  Třída PrikazRda implementuje pro hru příkaz rada.
 *  Tato třída je součástí jednoduché textové hry.
 *
 * @Project: zakladAdventuryIDEA
 * @Author: Petr Svoboda
 * @Ident: svop09
 */
public class PrikazRada implements IPrikaz {
    private static final String NAZEV = "rada";
    private HerniPlan plan;
    private Random rnd;
    private int pocetRad;
    private int limitDobreRady = 10;
    private int limitSpatneRady = 3;

    /**
     * Konstruktor
     *
     * @param plan vrací nám rady
     */
    public PrikazRada(HerniPlan plan) {
        this.plan = plan;
        this.pocetRad = 3;
        this.rnd = new Random();
    }

    /**
     * Vrátí nám radu, dle toho pokud je jídlo otrávené.
     *
     * @param parametry počet parametrů závisí na konkrétním příkazu.
     * @return zpráva, kterou vypíše hra hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Nevím s čím mám poradit";
        }

        String nazevJidla = parametry[0];
        Prostor aktualniProstor = plan.getAktualniProstor();

        if(nazevJidla.equals("počet")) {
            return "Zbývá ti: " + pocetRad + (pocetRad == 1 ? " rada" : " rady");
        } else if (!aktualniProstor.obsahujeJidlo(nazevJidla)) {
            return "Jídlo s názvem " + nazevJidla + " v prostoru neexistuje";
        } else if(pocetRad > 0) {
            Jidlo jidlo = aktualniProstor.vratJidlo(nazevJidla);
            String rada = "";
            pocetRad--;
            if (!jidlo.isOtravena()) {
                int n = rnd.nextInt(limitDobreRady);
                rada = plan.getRady().get(n);
                plan.getRady().remove(n);
                limitDobreRady--;
            } else {
                int n = rnd.nextInt(limitSpatneRady)+10;
                rada = plan.getRady().get(n);
                plan.getRady().remove(n);
                limitSpatneRady--;
            }
            return "O jídle: " + nazevJidla + " ti můžu říci jen: \n" + rada;
        } else {
            return "Došli ti rady";
        }
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }
}
