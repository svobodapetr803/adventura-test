package cz.vse.svoboda.logika;


import java.util.HashMap;

/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 */
public class HerniPlan {
    
    private Prostor aktualniProstor;
    private HashMap<Integer,String> rady;
    private HashMap<String,Prostor> prostory;
    
     /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        zalozProstoryHry();
    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
        Prostor restaurace = new Prostor("restaurace","klasická restaurace, kde každý dobře nají.");
        Prostor hospoda = new Prostor("hospoda","hospoda, která zavírá až všichni odejdou");
        Prostor bar = new Prostor("bar","bar Rébus, který nikdy nezavírá. Proč Rébus? To je ten Rébus.");
        Prostor pizzeria = new Prostor("pizzeria","pizzeria, přec jen každý miluje pizzu.");
        Prostor cukrarna = new Prostor("cukrárna", "nejlepší cukrárna ve městě, kde pečou ty nejlepší dorty.");
        Prostor jidelna = new Prostor("jídelna","klasická školní jídelna, najíst se tady dá, ale žádný zázrak nečekej.");
        Prostor bufet = new Prostor("bufet","školní bufet, kde najdeš zde vše možné.");
        Prostor bistro = new Prostor("bistro", "bistro, najdeš zde pokrmy z celého světa.");
        Prostor kavarna = new Prostor("kavárna", "kavárna. Kdo by si nedal nějaký dortíček a k tomu kafíčko");
        Prostor mcdonald = new Prostor("McDonalds", "Mekáč jak má být.");
        
        // přiřazují se průchody mezi prostory (sousedící prostory)
        restaurace.setVychod(hospoda);
        hospoda.setVychod(restaurace);
        hospoda.setVychod(bar);
        bar.setVychod(hospoda);
        bar.setVychod(pizzeria);
        pizzeria.setVychod(bar);
        pizzeria.setVychod(cukrarna);
        cukrarna.setVychod(pizzeria);
        cukrarna.setVychod(jidelna);
        jidelna.setVychod(cukrarna);
        jidelna.setVychod(bufet);
        bufet.setVychod(jidelna);
        bufet.setVychod(bistro);
        bistro.setVychod(bufet);
        bistro.setVychod(kavarna);
        kavarna.setVychod(bistro);
        kavarna.setVychod(mcdonald);
        mcdonald.setVychod(kavarna);

        //Vytvoření rad, 0-9 jsou dobré, 10-12 špatné
        rady = new HashMap<>();
        rady.put(0,"Přestože toto jídlo nevypadá moc lákavě, nebál bych se ho sníst");
        rady.put(1,"Vypadá lahodně, určitě bude v pořádku.");
        rady.put(2,"Hmm, risk je zisk.");
        rady.put(3,"Nevoní nejlépe, ale i tak si pochutnám.");
        rady.put(4,"Mňam!");
        rady.put(5,"Toho bych se nebál.");
        rady.put(6,"Nezbývá nic jiného než si pochutnat.");
        rady.put(7,"Jo tak to nevím.");
        rady.put(8,"Zkus a uvidíš.");
        rady.put(9,"Neboj se, už stačí jen pár soust.");
        rady.put(10,"No předtím by utekl snad každý.");
        rady.put(11,"Fuj, blee, to smrdí.");
        rady.put(12,"Zelená oblaka okolo leccos napovídají.");

        // vytvoření jídel a přidání jich do prostorů
        Jidlo vyvar = new Jidlo("vývar","kuřecí vývar jako od babičky",false);
        Jidlo svickova = new Jidlo("svíčková","Klasiká svíčková se šesti",true);
        Jidlo steak = new Jidlo("steak","krvavý steak medium rare s bramborami",false);
        Jidlo pivo = new Jidlo("pivo","pár vychlazených 12° nikdy neuškodí",false);
        Jidlo utopenec = new Jidlo("utopenec","k pivu jedině utopence",false);
        Jidlo nakladacky = new Jidlo("nakládačky","nakládačky v nálevu",true);
        Jidlo mojito = new Jidlo("mojito", "velmi silné Mojito",false);
        Jidlo arasidy = new Jidlo("arašídy", "slané arašídy v mističce",true);
        Jidlo chipsy = new Jidlo("chipsy","jemně pálivé chipsy",false);
        Jidlo hawaii = new Jidlo("hawaii","pizza hawaii, ananas na pizzu patří",true);
        Jidlo margherita = new Jidlo("margherita","Klasická pizza Margherita, jen tomato, sýr a těsto",false);
        Jidlo sunkova = new Jidlo("šunková","šunková pizza, jen šunka tam jen navíc",false);
        Jidlo tunakova = new Jidlo("tuňáková","pizza s kousky tuňáka",false);
        Jidlo tvaroh = new Jidlo("tvaroh","tvarohový dort ozdobenýn sypaným kokosem",false);
        Jidlo tiramisu = new Jidlo("tiramisu","vynikající italský dort tiramisu",false);
        Jidlo ovocny = new Jidlo("ovocný","ovocný dort z tropických plodů",true);
        Jidlo sacher = new Jidlo("sacher","sacher dort z tmavé čokolády",false);
        Jidlo rizek = new Jidlo("řízek","do zlatova smažený kuřecí řízek s kaší",false);
        Jidlo vrabec = new Jidlo("vrabec","moravský vrabecs s baby karotkou, bramborami",false);
        Jidlo rajska = new Jidlo("rajská","rajská polávke s písmenky",false);
        Jidlo spenatova = new Jidlo("špenátová","śpenátová polévka ze špenátu",true);
        Jidlo stripsy = new Jidlo("stripsy","obložená bageta s kuřecími stripsy a tatarkou",false);
        Jidlo golf = new Jidlo("golf","obložená bageta Golf s kuřecími nugetkami",false);
        Jidlo smazak = new Jidlo("smažák","smažený sýr v bulce se zelím",true);
        Jidlo mars = new Jidlo("marska","klasická marska s karamelem",false);
        Jidlo twix = new Jidlo("twixky","pár twixek, které vždy rychle zaženou hlad",false);
        Jidlo bramboraky = new Jidlo("bramboraky","hezké smažené bramboráky",false);
        Jidlo kuskus = new Jidlo("kuskus","kuskus s kousky zeleniny",false);
        Jidlo burgul = new Jidlo("burgul","burgul z popraskané předvařené krupice", true);
        Jidlo nudle = new Jidlo("nudle", "pražený rýžové nudle s hovězím masem", false);
        Jidlo phoga = new Jidlo("phoga","legendární vietnamská polévka ze silného kuřecího vývaru",false);
        Jidlo muffin = new Jidlo("muffin","muffin s jablky a drobenkou",false);
        Jidlo croissant  = new Jidlo("croissant","lehký croissant s meruňkovou náplní",false);
        Jidlo bagel = new Jidlo("bagel","ceasar bagel", true);
        Jidlo brownies = new Jidlo("brownies","čokoládový brownies se šlehačkou", false);
        Jidlo cookies = new Jidlo("cookies","cookies sušenky s kousky čokolády",false);
        Jidlo bigmac = new Jidlo("bigmac","klasický bigmac burger",false);
        Jidlo mcchicken = new Jidlo("mcchicken","McChicken se salátkem a majonézou",false);
        Jidlo chickenburger = new Jidlo("chickenburger","chickenburger v housce se sladkou chilli omáčkou",false);
        Jidlo cheeseburger = new Jidlo("cheeseburger","cheesburger, sýr, cibulka a nakládaná okurka", false);
        Jidlo nugetky = new Jidlo("nugetky","Dozlatova usmažené kousky z kuřecích prsíček",true);
        Jidlo hamburger = new Jidlo("hamburger","prostě hambáč z hovězího masa",true);
        Jidlo mcflurry = new Jidlo("mcflurry","McFlurry s lentikami a karamelem",false);

        restaurace.pridejJidlo(vyvar);
        restaurace.pridejJidlo(svickova);
        restaurace.pridejJidlo(steak);
        hospoda.pridejJidlo(pivo);
        hospoda.pridejJidlo(utopenec);
        hospoda.pridejJidlo(nakladacky);
        bar.pridejJidlo(mojito);
        bar.pridejJidlo(arasidy);
        bar.pridejJidlo(chipsy);
        pizzeria.pridejJidlo(hawaii);
        pizzeria.pridejJidlo(margherita);
        pizzeria.pridejJidlo(sunkova);
        pizzeria.pridejJidlo(tunakova);
        cukrarna.pridejJidlo(tvaroh);
        cukrarna.pridejJidlo(tiramisu);
        cukrarna.pridejJidlo(ovocny);
        cukrarna.pridejJidlo(sacher);
        jidelna.pridejJidlo(rizek);
        jidelna.pridejJidlo(vrabec);
        jidelna.pridejJidlo(rajska);
        jidelna.pridejJidlo(spenatova);
        bufet.pridejJidlo(stripsy);
        bufet.pridejJidlo(golf);
        bufet.pridejJidlo(smazak);
        bufet.pridejJidlo(mars);
        bufet.pridejJidlo(twix);
        bistro.pridejJidlo(bramboraky);
        bistro.pridejJidlo(kuskus);
        bistro.pridejJidlo(burgul);
        bistro.pridejJidlo(nudle);
        bistro.pridejJidlo(phoga);
        kavarna.pridejJidlo(muffin);
        kavarna.pridejJidlo(croissant);
        kavarna.pridejJidlo(bagel);
        kavarna.pridejJidlo(brownies);
        kavarna.pridejJidlo(cookies);
        mcdonald.pridejJidlo(bigmac);
        mcdonald.pridejJidlo(mcchicken);
        mcdonald.pridejJidlo(chickenburger);
        mcdonald.pridejJidlo(cheeseburger);
        mcdonald.pridejJidlo(nugetky);
        mcdonald.pridejJidlo(hamburger);
        mcdonald.pridejJidlo(mcflurry);

        aktualniProstor = restaurace;  // hra začíná v restauraci
    }
    
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
       aktualniProstor = prostor;
    }

    /**
     * Matoda vrací všechny rady
     *
     * @return rady
     */
    public HashMap<Integer, String> getRady(){return rady;}

}
