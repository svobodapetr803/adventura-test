package cz.vse.svoboda.logika;

import cz.vse.svoboda.uiText.TextoveRozhrani;

/**
 *  Třída PrikazZnovu implementuje pro hru příkaz znovu.
 *  Tato třída je součástí jednoduché textové hry.
 *
 * @Project: zakladAdventuryIDEA
 * @Author: Petr Svoboda
 * @Ident: svop09
 */
public class PrikazZnovu implements IPrikaz {
    private static final String NAZEV = "znovu";
    private Hra hra;
    private TextoveRozhrani ui;

    /**
     * Konstruktor
     *
     * @param hra restartování současné hry
     */
    public PrikazZnovu(Hra hra) {
        this.hra = hra;
    }

    /**
     * Restartuje a vytvoří novou hru
     *
     * @param parametry počet parametrů závisí na konkrétním příkazu.
     * @return zpráva, kterou vypíše hra hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length > 0) {
            return "Hrát znovu co? Nechápu, proč jste zadal druhé slovo.";
        }
        else {
            hra = new Hra();
            ui = new TextoveRozhrani(hra);
            ui.hraj();
            return "Hraješ od začátku.";
        }
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }
}
